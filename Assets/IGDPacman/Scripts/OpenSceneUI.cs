﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OpenSceneUI : MonoBehaviour {

    public Animator FadeOutAnimator;
    
    IEnumerator Start() {
        while (!Input.GetKeyDown(KeyCode.Return)) {
            yield return null;
        }

        FadeOutAnimator.Play("FadeOut");
        yield return new WaitForSeconds(1f);

		GameMenager.Instance.GoToNextLevel ();
    }
}
