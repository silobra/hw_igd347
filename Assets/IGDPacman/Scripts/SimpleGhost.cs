﻿using UnityEngine;
using System.Collections;

public class SimpleGhost : MonoBehaviour {

	public float speed;
	private Vector2 destination;
	private Vector2 direction;

	void Awake () {
		destination = Vector2.zero;
		direction = Vector2.zero;

	}
	// Use this for initialization
	void Start () {
		direction = Vector2.right;
		destination = transform.position;

	}

	// Update is called once per frame
	void FixedUpdate () {

		Vector2 p = Vector2.MoveTowards (transform.position, destination, speed);
		GetComponent<Rigidbody2D> ().MovePosition (p);

		if (Vector2.Distance(destination, transform.position) < 0.00001f) {

			if (Valid (Vector2.right)) {
				direction = Vector2.right;
			} else if (Valid (Vector2.up)) {
				direction = Vector2.up;
			} else if (Valid (Vector2.down)) {
				direction = Vector2.down;
			} else if (Valid (Vector2.left)) {
				direction = Vector2.left;
			} else {
				direction = Vector2.zero;
			}

			destination = (Vector2)transform.position + direction;
		}

	}

	bool Valid(Vector2 direction) {

		Vector2 pos = transform.position;
		direction += new Vector2(direction.x * 0.45f, direction.y * 0.45f);
		RaycastHit2D hit = Physics2D.Linecast(pos + direction, pos);
		if (hit.collider == null) {
			// Hit nothing
			return true;
		}
		else {
			if (hit.transform == this.transform) {
				return true;
			} else if (hit.transform.CompareTag ("Item")) {
				return true;
			} else if (hit.transform.CompareTag ("Energizer")) {
				return true;
			} else if (hit.transform.CompareTag ("Player")) {
				return true;
			} else {
				return false;
			}
		}
	}
}
