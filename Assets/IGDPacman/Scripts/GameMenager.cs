﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameMenager : MonoBehaviour {



	public string[] AllLevelSceneNames;
	public int CurrentLevel;
	public static GameMenager Instance;
	public int pacmanLife = 3;

	void Awake() {

		if (Instance != null) {
			Destroy(this.gameObject);
			return;

		}

		Instance = this;
		DontDestroyOnLoad(this.gameObject);

	}
	public void GoToNextLevel () {
		pacmanLife = 3;
		CurrentLevel++;
		string sceneName = AllLevelSceneNames [CurrentLevel];
		SceneManager.LoadScene (sceneName);

	}

	public void RestartCurrentLevel() {

		if (pacmanLife > 0) {
			string currentLevelSceneName = AllLevelSceneNames [CurrentLevel];
			SceneManager.LoadScene (currentLevelSceneName);
			pacmanLife--;

		} else {
			pacmanLife = 3;
			CurrentLevel = 0;
			SceneManager.LoadScene ("OpenScene");
		}
	}
		
	void Update () {
		#if UNITY_EDITOR
		if (Input.GetKey (KeyCode.LeftShift) && Input.GetKey (KeyCode.LeftControl) && Input.GetKeyDown (KeyCode.R)) 
			GoToNextLevel();
		#endif
	}
}