﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class GhostController : MonoBehaviour {
    
    public float speed = 0.4f;
    public Map CurrentMap;
    public PacmanController Pacman;

    private Vector2 destination;

    private List<Tile> mPath;

    void Awake() {
        destination = Vector2.zero;
    }

    // Use this for initialization
    void Start() {
        destination = transform.position;
    }

    void FixedUpdate() {
        // move closer to destination
        Vector2 p = Vector2.MoveTowards(transform.position, destination, speed);
        GetComponent<Rigidbody2D>().MovePosition(p);
        
        // if pacman is in the center of a tile
        if (Vector2.Distance(destination, transform.position) < 0.00001f) {
            int ghostTilePosX = Mathf.FloorToInt(transform.position.x);
            int ghostTilePosY = Mathf.FloorToInt(transform.position.y);
            Tile ghostTile = CurrentMap.GetTileAt(ghostTilePosX, ghostTilePosY);

            int pacmanTilePosX = Mathf.FloorToInt(Pacman.transform.position.x);
            int pacmanTilePosY = Mathf.FloorToInt(Pacman.transform.position.y);
            Tile pacmanTile = CurrentMap.GetTileAt(pacmanTilePosX, pacmanTilePosY);
            
			if (ghostTile.NeighbourCount >= 2) {

				mPath = CurrentMap.FindShortestPath (ghostTile, pacmanTile);

				if (mPath.Count > 1) {

					destination = new Vector2 (mPath [1].PosX, mPath [1].PosY);

				}
			}
        }
    }

    void OnDrawGizmos() {
        if (mPath == null) {
            return;
        }
        if (mPath.Count == 0) {
            return;
        }

        Gizmos.color = Color.green;
        for (int i = 0; i < mPath.Count - 1; i++) {
            Gizmos.DrawLine(new Vector3(mPath[i].PosX, mPath[i].PosY, 0),
                            new Vector3(mPath[i+1].PosX, mPath[i+1].PosY, 0));
        }
    } 

	void OnTriggerEnter2D (Collider2D other) {
		if (other.gameObject.CompareTag("Player") == true) {
			GameMenager.Instance.RestartCurrentLevel ();

		}
	}
}
