﻿using UnityEngine;
using System.Collections;


[System.Serializable]

public class Tile {

    public int PosX;
    public int PosY;
    public bool Movable;

    public int NeighbourCount;
    public Tile Left;
    public Tile Right;
    public Tile Up;
    public Tile Down;

    public int TraversalPoint;

    public Tile(int x, int y) {
        PosX = x;
        PosY = y;
        Movable = false;

        NeighbourCount = 0;
        Left = null;
        Right = null;
        Up = null;
        Down = null;

        TraversalPoint = int.MaxValue;
    }

}
