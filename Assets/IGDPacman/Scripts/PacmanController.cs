﻿using UnityEngine;
using System.Collections;

public class PacmanController : MonoBehaviour {

    public float speed;
	public int NumberOfCollectedPacdots = 0;
	public int NumberOfTotalPacdots = 0;

    private Vector2 destination;
    private Vector2 direction;
    private Vector2 nextDirection;
	//private Animator pacmanAnimator;

    void Awake() {
        destination = Vector2.zero;
        direction = Vector2.zero;
        nextDirection = Vector2.zero;
    }

    // Use this for initialization
    void Start () {
        direction = Vector2.right; // or direction = new Vector2(1, 0);
        destination = transform.position;
		NumberOfCollectedPacdots = 0;

		GameObject[] allPacdots = GameObject.FindGameObjectsWithTag ("Item");
		NumberOfTotalPacdots = allPacdots.Length;
		for (int i = 0; i < allPacdots.Length; i++) {
			Debug.Log ("=> " + allPacdots [i].name);
		}
    }
	
    // Update is called once per frame
    void FixedUpdate () {
        // move closer to destination
        Vector2 p = Vector2.MoveTowards(transform.position, destination, speed);
        GetComponent<Rigidbody2D>().MovePosition(p);

        if (Input.GetAxis("Horizontal") > 0) {
            nextDirection = Vector2.right; // or nextDirection = new Vector2(1, 0);
        }
        else if (Input.GetAxis("Horizontal") < 0) {
            nextDirection = Vector2.left; // or nextDirection = new Vector2(-1, 0);
        }
        else if (Input.GetAxis("Vertical") > 0) {
            nextDirection = Vector2.up; // or nextDirection = new Vector2(0, 1);
        }
        else if (Input.GetAxis("Vertical") < 0) {
            nextDirection = Vector2.down; // or nextDirection = new Vector2(0, -1);
        }

        // if pacman is in the center of a tile
        if (Vector2.Distance(destination, transform.position) < 0.00001f) {
			
            if (Valid(nextDirection)) {
                destination = (Vector2)transform.position + nextDirection;
                direction = nextDirection;
				/*pacmanAnimator.SetFloat ("DirX", direction.x);
				pacmanAnimator.SetFloat ("DirY", direction.y);*/
            }
            else {
                direction = direction;
                if (Valid(direction)) {
                  	destination = (Vector2)transform.position + direction;
                }
            }
        }
    }
    
    bool Valid(Vector2 direction) {
		
        Vector2 pos = transform.position;
        direction += new Vector2(direction.x * 0.45f, direction.y * 0.45f);
        RaycastHit2D hit = Physics2D.Linecast(pos + direction, pos);
        if (hit.collider == null) {
            // Hit nothing
            return true;
        }
		else {
			if (hit.transform == this.transform) {
				return true;
			} else if (hit.transform.CompareTag ("Item")) {
				return true;
			} else if (hit.transform.CompareTag ("Energizer")) {
				return true;
			} else {
                return false;
            }
        }
    }
	void OnTriggerEnter2D (Collider2D other) {
		if (other.gameObject.CompareTag("Item") == true ) {
			Destroy (other.gameObject);
			NumberOfCollectedPacdots++;
			if (NumberOfCollectedPacdots >= NumberOfTotalPacdots) {
				GameMenager.Instance.GoToNextLevel ();
			}
		}
	}
}
