﻿using UnityEngine;
using System.Collections;
using System.IO;

using System.Collections.Generic;


public class Map : MonoBehaviour {
	public List<Tile> Tiles = new List<Tile>();
	public string MapFilename = "";
	public int Width = 28;
	public int Height = 31;

	void Awake() {
		// Read map data from file
		string mapData = "";
		// File name => "StreamingAssets/<MapFileName>"
		string filenamewithFullpath = Application.streamingAssetsPath + "/" +
			MapFilename;
		StreamReader reader = new StreamReader(filenamewithFullpath);
		mapData = reader.ReadToEnd();
		reader.Close();
		// Create all tiles
		int y = Height;
		string[] lines = mapData.Split('\n');
		for (int l = 0; l < lines.Length; l++) {
			int x = 1;
			string line = lines[l];
			for (int c = 0; c < line.Length; c++) {
				Tile tile = new Tile(x, y);
				if (line[c] == '0') {
					tile.Movable = false;
				}
				else if (line[c] == '1') {
					tile.Movable = true;
				}
				Tiles.Add(tile);
				x++;
			}
			y--;
		}
		// Connect neighbours
		for (int posX = 1; posX <= Width; posX++) {
			for (int posY = 1; posY <= Height; posY++) {
				Tile tile = GetTileAt(posX, posY);
				Tile leftTile = GetTileAt(posX - 1, posY);
				if (leftTile != null && leftTile.Movable) {
					tile.Left = leftTile;
					tile.NeighbourCount++;
				}
				Tile rightTile = GetTileAt(posX + 1, posY);
				if (rightTile != null && rightTile.Movable) {
					tile.Right = rightTile;
					tile.NeighbourCount++;
				}
				Tile upTile = GetTileAt(posX, posY + 1);
				if (upTile != null && upTile.Movable) {
					tile.Up = upTile;
					tile.NeighbourCount++;
				}
				Tile downTile = GetTileAt(posX, posY - 1);
				if (downTile != null && downTile.Movable) {
					tile.Down = downTile;
					tile.NeighbourCount++;
				}
			}
		}
	}
	public Tile GetTileAt(int x, int y) {
		for (int i = 0; i < Tiles.Count; i++) {
			if (Tiles[i].PosX == x && Tiles[i].PosY == y) 
			{
				return Tiles[i];
			}	
		}
		return null;
	}


	public List<Tile> FindShortestPath (Tile fromTile, Tile toTile)
	{

		// Reset all cell's traversal point

		for (int x = 1; x <= Width; x++) {

			for (int y = 1; y <= Height; y++) {

				Tile tile = GetTileAt (x, y);

				tile.TraversalPoint = int.MaxValue;

			}

		}

		Stack<Tile> tileStack = new Stack<Tile> ();

		fromTile.TraversalPoint = 0;

		tileStack.Push (fromTile);

		// 1. Fill Traversal point until toTile found!

		while (tileStack.Count > 0) {

			Tile tile = tileStack.Pop ();

			int v = tile.TraversalPoint + 1;

			if (tile.Up != null) {

				if (tile.Up.TraversalPoint > v) {

					tile.Up.TraversalPoint = v;

					if (tile.Up != toTile) {

						tileStack.Push (tile.Up);

					}

				}

			}

			if (tile.Right != null) {

				if (tile.Right.TraversalPoint > v) {

					tile.Right.TraversalPoint = v;

					if (tile.Right != toTile) {

						tileStack.Push (tile.Right);

					}

				}

			}

			if (tile.Down != null) {

				if (tile.Down.TraversalPoint > v) {

					tile.Down.TraversalPoint = v;

					if (tile.Down != toTile) {

						tileStack.Push (tile.Down);

					}

				}

			}

			if (tile.Left != null) {

				if (tile.Left.TraversalPoint > v) {

					tile.Left.TraversalPoint = v;

					if (tile.Left != toTile) {

						tileStack.Push (tile.Left);

					}

				}

			}

		}
		List<Tile> path = new List<Tile> ();

		Tile traverseBackTile = toTile;

		path.Add (traverseBackTile);

		while (traverseBackTile != fromTile) {

			Tile[] neightbourTiles = new Tile[] {

				traverseBackTile.Up,

				traverseBackTile.Right,

				traverseBackTile.Down,

				traverseBackTile.Left

			};

			// Find minimum traversal point tile

			Tile minTraversePointTile = null;

			int minTraversePoint = int.MaxValue;

			for (int i = 0; i < neightbourTiles.Length; i++) {

				if (neightbourTiles [i] == null) {

					continue;

				}

				if (neightbourTiles [i].TraversalPoint < minTraversePoint) {

					minTraversePoint = neightbourTiles [i].TraversalPoint;

					minTraversePointTile = neightbourTiles [i];

				}

			}

			traverseBackTile = minTraversePointTile;

			path.Add (traverseBackTile);

		}
	
		path.Reverse();
		return path;
	}


	void OnDrawGizmos() {
		if (Tiles.Count == 0) {
			return;
		}
		Gizmos.color = new Color(1, 1, 1, 0.2f);
		for (int posX = 1; posX <= Width; posX++) {
			for (int posY = 1; posY <= Height; posY++) {
				Tile tile = GetTileAt(posX, posY);
				if (tile.Movable == false) {
					continue;
				}
				if (tile.Left != null) {
					Vector3 p1 = new Vector3(tile.PosX, tile.PosY, 0);
					Vector3 p2 = new Vector3(tile.Left.PosX, tile.Left.PosY, 0);
					Gizmos.DrawLine(p1, p2);
				}
				if (tile.Right != null) {
					Vector3 p1 = new Vector3(tile.PosX, tile.PosY, 0);
					Vector3 p2 = new Vector3(tile.Right.PosX, tile.Right.PosY, 0);
					Gizmos.DrawLine(p1, p2);
				}
				if (tile.Up != null) {
					Vector3 p1 = new Vector3(tile.PosX, tile.PosY, 0);
					Vector3 p2 = new Vector3(tile.Up.PosX, tile.Up.PosY, 0);
					Gizmos.DrawLine(p1, p2);
				}
				if (tile.Down != null) {
					Vector3 p1 = new Vector3(tile.PosX, tile.PosY, 0);
					Vector3 p2 = new Vector3(tile.Down.PosX, tile.Down.PosY, 0);
					Gizmos.DrawLine(p1, p2);
				}
			}
		}
	}
}